# ETL Data Pipeline
## Project Overview
This project focuses on creating a ETL (Extract, Transform, Load) data pipeline for processing and scheduling sales data using python and prefect. Prefect is an open-source workflow management system, to design, execute, monitor, and debug the ETL pipelines. The primary objectives of this project are as follows:

- Data Extraction: Extracting data from API.
- Data Transformation: Applying necessary transformations to clean the data.
- Data Loading: Loading the processed data into datalake.
- Prefect Integration: Leveraging Prefect to create, schedule, and monitor the ETL workflows.

## Scheduling with Prefect

## Things I Learnt

Throughout the course of this project, I gained valuable knowledge and experience in the following areas:

- **Prefect Workflow Management**: I learned how to design and orchestrate complex ETL workflows using Prefect. This includes defining tasks, dependencies, and scheduling for efficient data processing.

- **Data Extraction Techniques**: I explored various methods for extracting data from diverse sources, such as databases, APIs, and flat files, and integrated them into the pipeline.

- **Data Transformation**: I acquired skills in data transformation, including data cleaning, aggregation, and enrichment to prepare data for analysis.

- **Error Handling and Logging**: I implemented error-handling mechanisms and logging practices to monitor pipeline executions and facilitate debugging in a production environment.

- **Scheduling and Automation**: I learned how to schedule ETL workflows to automate data extraction and transformation tasks, improving the efficiency of the data pipeline.


