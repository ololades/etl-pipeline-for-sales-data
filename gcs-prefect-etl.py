import prefect
from prefect import task, flow
import pandas as pd
from prefect_gcp.cloud_storage import GcsBucket
import requests
import io



# Task to extract data from a URL
@task()
def extract_data(source_url: str) -> pd.DataFrame:
    """Extracts data from a URL"""
    response = requests.get(source_url)
    response.raise_for_status()  # Raise an exception for non-200 responses

    # Create a Pandas DataFrame directly from the response content
    df = pd.read_csv(io.StringIO(response.text))
    return df


# Define the transform_data task
@task()
def transform_data(df: pd.DataFrame) -> pd.DataFrame:
    """Transforms data by removing missing values and changing data types"""

    df['YEAR BUILT'] = pd.to_datetime(df['YEAR BUILT'])
    df['SALE DATE'] = pd.to_datetime(df['SALE DATE'])
    print(df.head(2))
    print(f"columns: {df.dtypes}")
    print(f"shape: {df.shape}")
    print(f"rows: {len(df)}")
    
    return df


# Task to upload a local csv file to GCS
@task()
def load_gcs(df: pd.DataFrame, bucket_name: str, local_path: str) -> None:
    """Upload local csv file to GCS"""
    gcs_block = GcsBucket.load(bucket_name)
    
    # Write the DataFrame to a local CSV file
    df.to_csv(local_path, index=False)
    
    # Upload the local CSV file to GCS
    with open(local_path, 'rb') as file:
        gcs_block.upload_from_file_object(file, local_path, timeout=360)
    
    return


# Define the Prefect flow
@flow() #log_metadata=True, metadata={"job_id": 12345}
def etl_web_to_gcs(chunk_size: int = 50000) -> None:
    """The main ETL function"""
    source_url = 'https://data.cityofnewyork.us/api/views/w2pb-icbu/rows.csv?accessType=DOWNLOAD'
    local_path = "sales_data.csv"  # Specify the local file path
    bucket_name= "etl-gcs"  # GCS bucket name

    df = extract_data(source_url)
       # Iterate over the data in chunks
    for i in range(0, len(df), chunk_size):
        df_chunk = df[i:i + chunk_size]

        # Process the current chunk of data
        df_chunk_transformed = transform_data(df_chunk)

        # Upload the processed chunk of data to the cloud
        load_gcs(df_chunk_transformed, bucket_name, local_path)



# Entry point for running the Prefect flow to initialize the ETL process
if __name__ == "__main__":
    etl_web_to_gcs()
